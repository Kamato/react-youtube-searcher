# React-YouTube-Searcher

A simple app built using React with ES6 that can search for YouTube videos.

![screenshot][1]


###Getting Started###

1. Install dependencies.

        npm install
        
2. Start webpack-dev-server.

        npm start
        
3. Navigate your browser to:
        
        localhost:8080
        

[1]: https://raw.githubusercontent.com/panteng/react-youtube-searcher/master/screenshot.jpg